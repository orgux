/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* constants needed by core and precompute */

#define PI 3.141592653589
#define PI2 PI*2

#define HARMONIC_COUNT 8

#define BASE_CONFIG 8
#define FM_CONFIG 9
#define FM_FREQ_CONFIG 10
#define LP_CONFIG 11
#define HP_CONFIG 12

#define COMPONENT_MAX 255


#define SINE_BASE 128
#define SQUARE_BASE 129
#define TRIANGLE_BASE 130
#define SAW_BASE 131
#define NOISE_BASE 132

#define FM_OFF 133

#define SEQUENCE_MAX 4096

#define SAMPLE_LENGTH (1<<16)
#define SAMPLE_COUNT 16
#define SAMPLE_NOTE_ZERO 60

#define ANTIPOP_LENGTH 512
#define WAVEFORM_LENGTH (1<<10)

#define MAX_COMMANDS 256
#define MAX_NOTES 128

#define RANDF() (((float)rand())/RAND_MAX)
#define RAND_SIGNAL() (RANDF()*2 - 1)