/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>



#include "orgux.h"

#include "constants.h"
#include "instruments.h"





extern void precompute(int sample_rate);
int precompute_ok = 0;



float square_wave[WAVEFORM_LENGTH];
float saw_wave[WAVEFORM_LENGTH];
float triangle_wave[WAVEFORM_LENGTH];

float waveform[128][WAVEFORM_LENGTH];
float waveform_M[128][WAVEFORM_LENGTH];
float waveform_Z[128][WAVEFORM_LENGTH];

float samples[SAMPLE_COUNT][SAMPLE_LENGTH];
int sample_len[SAMPLE_COUNT];
float antipop[ANTIPOP_LENGTH];

struct orgux_state{
  float volume[16];
  float pan_l[16];
  float pan_r[16];
  float bend[16];
  int instrument[16];

  int sequence[SEQUENCE_MAX];
  unsigned char seq_events[SEQUENCE_MAX][3];
  int seq_en;
  int seq_ptr;
  int seq_len;
  int seq_tick;
  int seq_samples;
  int loop_start;
  int loop_end;

  int sample_rate;
  float lp_alpha;

  int command_queue[512][5]; /* sample type chan val1 val2 */
  int command_count;

  int note_on[MAX_NOTES];
  int note_channel[MAX_NOTES];
  int note_note[MAX_NOTES];
  float velocity[MAX_NOTES];
  int antipop_flag[MAX_NOTES];
  int antipop_ptr[MAX_NOTES];
  float wave_ptr[MAX_NOTES];
  float wave_step[MAX_NOTES];
  int sample_ptr[MAX_NOTES];
  float lp_y[MAX_NOTES];
  int note_max;
};

#define SWAP(X,Y,T) {T tmp = X; X = Y; Y = tmp;}
void note_swap(orgux_state* S, int i1, int i2){

  SWAP(S->note_on[i1], S->note_on[i2], int);
  SWAP(S->note_channel[i1], S->note_channel[i2], int);
  SWAP(S->note_note[i1], S->note_note[i2], int);
  SWAP(S->velocity[i1], S->velocity[i2], float);
  SWAP(S->antipop_flag[i1], S->antipop_flag[i2], int);
  SWAP(S->antipop_ptr[i1], S->antipop_ptr[i2], int);
  SWAP(S->wave_ptr[i1], S->wave_ptr[i2], float);
  SWAP(S->wave_step[i1], S->wave_step[i2], float);
  SWAP(S->sample_ptr[i1], S->sample_ptr[i2], int);

}

void orgux_note_on(orgux_state* S, int chan, int note, int velocity){

  int i=S->note_max;

  if(i == MAX_NOTES) return;

  S->note_on[i] = 1;
  S->note_channel[i] = chan;
  S->note_note[i] = note;
  S->velocity[i] = ((float)velocity)/127;
  S->antipop_flag[i] = -1;
  S->antipop_ptr[i] = 0;
  S->wave_ptr[i] = 0.0f;
  S->sample_ptr[i] = 0;
  //S->wave_step[i] = (int)((440*pow(2,(note-69)/12.0)*WAVEFORM_LENGTH)/S->sample_rate);
  S->wave_step[i] = (440*pow(2,(note-69)/12.0)*WAVEFORM_LENGTH)/S->sample_rate;
  S->note_max++;

//printf("note on i=%d\n",i);
for(int j=0; j<S->note_max; j++){
  //printf("[%d %d %d %d]\n",S->note_on[j], S->note_channel[j], S->note_note[j], S->antipop_flag[j]);
}

}

void orgux_note_off(orgux_state* S, int chan, int note){

  for(int i=0; i < S->note_max; i++){
    if(S->note_on[i]==1 && S->note_channel[i]==chan && S->note_note[i]==note && S->antipop_flag[i]<1){

      S->antipop_flag[i] = 1;

//printf("note end\n");
for(int j=0; j<S->note_max; j++){
  //printf("[%d %d %d %d]\n",S->note_on[j], S->note_channel[j], S->note_note[j], S->antipop_flag[j]);
}

      break;
    }
  }

}

void orgux_change_volume(orgux_state* S, int chan, int value){
  //printf("change volume %d %d\n", chan, value);
  S->volume[chan] = ((float)value)/127;
}

void orgux_change_pan(orgux_state* S, int chan, int value){
  //printf("change pan %d %d\n", chan, value);
  float P = (((float)value)/127) * 2 - 1;

  if(P > 0){
    S->pan_l[chan] = 1.0f - P;
    S->pan_r[chan] = 1.0f;
  }
  else{
    S->pan_l[chan] = 1.0f;
    S->pan_r[chan] = P + 1.0f;
  }
}

void orgux_change_instrument(orgux_state* S, int chan, int value){
  //printf("change instrument %d %d\n", chan, value);
  S->instrument[chan] = value;
}

void orgux_pitch_bend(orgux_state* S, int chan, int bend){
  S->bend[chan] = ((((float)bend)/16383) * 2 - 1) * 1;
}




void orgux_process(orgux_state* S, float* lbuf, float* rbuf, int len){

  int C=0;

  int swap_ok = 0;

  if(S->seq_en){//schedule some events
/*
    double ticks_per_period = len*ticks_per_sample;
    double next_tick = cur_tick + ticks_per_period;

    double samples_per_tick = 1;

    while(S->sequence[S->seq_ptr] <= next_tick){
      int N = S->seq_ptr;
      int target_sample = samples_per_tick*(S->sequence[N] - cur_tick);
      orgux_schedule(S, target_sample, S->seq_events[N]);
      S->seq_ptr++;
      if(S->seq_ptr == S->seq_len){
        S->seq_en = 0;
        break;
      }
    }

    cur_tick = next_tick;
*/
  }

  for(int i=0; i<len; i++){

    while(i == S->command_queue[C][0] && C < S->command_count){
      orgux_control(S, S->command_queue[C][1],
                       S->command_queue[C][2],
                       S->command_queue[C][3],
                       S->command_queue[C][4]);
      C++;
    }

    lbuf[i] = 0.0f;
    rbuf[i] = 0.0f;

    int jmax = S->note_max;
    for(int j=0; ;j++){

      if(j >= jmax) break;

      if(!S->note_on[j]) continue;

      if(S->note_channel[j] == 9){
        int index = S->note_note[j] - SAMPLE_NOTE_ZERO;
        if(index < 0){index=0;}
        if(index >= SAMPLE_COUNT){index=SAMPLE_COUNT-1;}
        float y = samples[index][S->sample_ptr[j]++];
        if(S->sample_ptr[j] == SAMPLE_LENGTH || S->sample_ptr[j] == sample_len[j]){
          swap_ok = 1;
        }

        float V = S->volume[9];
        float A = S->velocity[j];
        lbuf[i] += V*A*S->pan_l[9]*y;
        rbuf[i] += V*A*S->pan_r[9]*y;

        //lbuf[i] += y;
        //rbuf[i] += y;

        if(swap_ok){
          note_swap(S, j, S->note_max-1);
          S->note_max--;
          j--;
          jmax--;
          swap_ok = 0;
        }

        continue;
      }
//continue;

      int I = S->instrument[S->note_channel[j]];
/* hard computations */
/*      float x = S->wave_ptr[j];
      float x1 = floor(x);
      float x2 = x1 + 1.0f;
      int X1 = (int)x1;
      int X2 = X1 + 1;
      if(X2 == WAVEFORM_LENGTH) X2 = 0;
      float y1 = waveform[I][X1];
      float y2 = waveform[I][X2];
      float m = (y2-y1)/(x2-x1);
      float y = m*(x-x1) + y1;
*/

      float x = S->wave_ptr[j];
      int X0 = (int)floor(x);
      float y = waveform_Z[I][X0] + waveform_M[I][X0]*x;

//float y = 0;

/*
      int I = S->instrument[S->note_channel[j]];
      int X = S->wave_ptr[j];
      float y = S->waveform[I][X];
  */    

      //float y0 = S->lp_y[j];
      //y = y0 + S->lp_alpha * (y - y0);
      //S->lp_y[j] = y;

      switch(S->antipop_flag[j]){
        case -1:
          y *= antipop[S->antipop_ptr[j]];
          S->antipop_ptr[j]++;

          if(S->antipop_ptr[j] == ANTIPOP_LENGTH){
            S->antipop_ptr[j] = ANTIPOP_LENGTH-1;
            S->antipop_flag[j] = 0;
          }
          break;
        case 1:
          y *= antipop[S->antipop_ptr[j]];
          S->antipop_ptr[j]--;
          if(S->antipop_ptr[j] < 0){
            swap_ok = 1;
          }
          break;
      }


      int chan = S->note_channel[j];
      float volume = S->volume[chan];
      float velocity = S->velocity[j];
      lbuf[i] += volume*velocity*S->pan_l[chan]*y;
      rbuf[i] += volume*velocity*S->pan_r[chan]*y;

if(lbuf[i]> 1.0 || lbuf[i] < -1.0){
printf("ehh lbuf[%d] = %f\n",i,lbuf[i]);
printf("%f %f %f %f %f\n",y,volume, velocity, S->pan_l[chan], S->pan_r[chan]);
}

      S->wave_ptr[j] += S->wave_step[j];
      while(S->wave_ptr[j] >= WAVEFORM_LENGTH){
        S->wave_ptr[j] -= WAVEFORM_LENGTH;
      }

      if(swap_ok){ /* kills note */
        note_swap(S, j, S->note_max-1);
        S->note_max--;
        j--;
        jmax--;
        swap_ok = 0;
//printf("note off\n");
for(int z=0; z<S->note_max; z++){
  //printf("[%d %d %d %d]\n",S->note_on[z], S->note_channel[z], S->note_note[z], S->antipop_flag[z]);
}
      }

    }

  }

  S->command_count = 0;

}




void orgux_schedule(orgux_state* S, int sample, unsigned char midi[3]){
  //orgux_control(S, midi[0]&0xf0, midi[0]&0x0f, midi[1], midi[2]);

  if(S->command_count == MAX_COMMANDS){
    return;
  }

  switch(midi[0]&0xf0){
    case 0xA0:
    case 0xD0:
      return;
  }
  /*
the queue is sorted by time (sample)
soonest times are to the beginning of the queue
queue[0] is the first element
insert: move all later events right one
  */

//printf("schedule %d %x %x %x\n",sample,midi[0],midi[1],midi[2]);
  int i;
  for(i=0; i<S->command_count; i++){

    if(sample < S->command_queue[i][0]){

      for(int j=S->command_count; j>i; j--){
        S->command_queue[j][0] = S->command_queue[j-1][0];
        S->command_queue[j][1] = S->command_queue[j-1][1];
        S->command_queue[j][2] = S->command_queue[j-1][2];
        S->command_queue[j][3] = S->command_queue[j-1][3];
        S->command_queue[j][4] = S->command_queue[j-1][4];
      }

      S->command_queue[i][0] = sample;
      S->command_queue[i][1] = midi[0]&0xf0;
      S->command_queue[i][2] = midi[0]&0x0f;
      S->command_queue[i][3] = midi[1];
      S->command_queue[i][4] = midi[2];

      break;
    }
  }

  if(i==S->command_count){//end of queue
    S->command_queue[i][0] = sample;
    S->command_queue[i][1] = midi[0]&0xf0;
    S->command_queue[i][2] = midi[0]&0x0f;
    S->command_queue[i][3] = midi[1];
    S->command_queue[i][4] = midi[2];
  }

  S->command_count++;
/*for(int i=0; i<S->command_count; i++){
printf("%d %d %d %d %d\n",
    S->command_queue[i][0],
    S->command_queue[i][1],
    S->command_queue[i][2],
    S->command_queue[i][3],
    S->command_queue[i][4]);

}*/

}

void orgux_control(orgux_state* S, int type, int chan, int val1, int val2){

  int bend = (val1 << 7) + val2;

  //printf("control %d %d %d %d\n", chan, type, val1, val2);

  switch(type){
    case 0x90:
      orgux_note_on(S, chan, val1, val2);
      return;
    case 0x80:
      orgux_note_off(S, chan, val1);
      return;
    case 0xC0:
      orgux_change_instrument(S, chan, val1);
      return;
    case 0xB0:
      if(val1 == 7){
        orgux_change_volume(S, chan, val2);
      }
      else if(val1 == 10){
        orgux_change_pan(S, chan, val2);
      }
      return;
    case 0xE0:
      orgux_pitch_bend(S, chan, bend);
      return;
  }
}



orgux_state* orgux_init(int sample_rate){
  orgux_state* S = malloc(sizeof(orgux_state));
  if(!S) return NULL;



  /*precompute waveforms*/
  if(!precompute_ok){

    precompute(sample_rate);
    precompute_ok = 1;
  
  }

  /* setup oscillator states */

  for(int i=0; i<16; i++){
    S->volume[i] = 1.0f;
    S->pan_l[i] = 1.0f;
    S->pan_r[i] = 1.0f;
  }

  for(int i=0; i<MAX_NOTES; i++){
    S->wave_ptr[i] = 0.0f;
    S->antipop_ptr[i] = 0;
    S->antipop_flag[i] = 0;
    S->note_on[i] = 0;
    S->sample_ptr[i] = 0;
    S->lp_y[i] = 0;
  }

  S->sample_rate = sample_rate;

  S->command_count = 0;
  S->note_max = 0;

  S->seq_en = 0;

  //S->lp_alpha = dt/(dt+0.001);

  return S;  

}

void orgux_free(orgux_state* S){
  free(S);
}



void orgux_seq_append(orgux_state* S, int tick, int type, int chan, int val1, int val2){
  if(S->seq_len == SEQUENCE_MAX) return;

  S->sequence[S->seq_len] = tick;
  S->seq_events[S->seq_len][0] = type | chan;
  S->seq_events[S->seq_len][1] = val1;
  S->seq_events[S->seq_len][2] = val2;
  S->seq_len++;
}

void orgux_seq_sort(orgux_state* S){

for(int i=0; i<S->seq_len; i++){
  printf("%d: %d %x %d %d\n",i, S->sequence[i], S->seq_events[i][0], S->seq_events[i][1], S->seq_events[i][2]);
}

}

void orgux_play(orgux_state* S, int yesno){
  S->seq_en = yesno;
}