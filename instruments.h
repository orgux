/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* configuration of instruments

you may combine the first eight harmonics in whatever proportions you wish
to make a carrier for your instrument. magnitudes are proportional, so
2,1,0,0,0... means two parts harmonic zero plus one part harmonic one, for
a total of three parts or 0.666*f_0 + 0.333*f_1. Actually the coefficients are
squared to compensate for lack of dynamic range (0-255).

you may choose sine, square, saw, triangle or noise as the basis function or
choose any previously defined instrument. choosing a future instrument will
result in an undefined instrument.

you may choose a frequency modulation signal the same way as you choose a basis.
FM_OFF is a special value to choose no FM signal. the value after the FM signal
is the frequency of the FM signal. The frequency is determined logarithmically
via F = 2^(value/8).

The next two values determine cutoff frequencies for the instruments low pass
and high pass filter. 0 deactivates the filter, otherwise F = 2^(value/8).

At this the the last four numbers are useless.
*/



const unsigned char instr_config[128][16] = {
/*harmonics
 0 1 2 3 4 5 6 7| basis      FM FM_f LP HP*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*0*/
{2,1,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{4,2,1,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{8,4,2,1,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0,  SQUARE_BASE,   FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0,  TRIANGLE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0,  SAW_BASE,      FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0,  NOISE_BASE,    FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SQUARE_BASE,  FM_OFF, 0, 0, 0,0,0,0},/*8*/
{2,1,0,0,0,0,0,0, SQUARE_BASE,  FM_OFF, 0, 0, 0,0,0,0},
{4,2,1,0,0,0,0,0, SQUARE_BASE,  FM_OFF, 0, 0, 0,0,0,0},
{8,4,2,1,0,0,0,0, SQUARE_BASE,  FM_OFF, 0, 0, 0,0,0,0},

{2,1,0,0,0,0,0,0, TRIANGLE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{2,0,1,0,0,0,0,0, TRIANGLE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{2,1,1,0,0,0,0,0, TRIANGLE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{3,1,1,0,0,0,0,0, TRIANGLE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{2,1,0,0,0,0,0,0, SAW_BASE, FM_OFF, 0, 0, 0,0,0,0},/*16*/
{4,0,1,0,0,0,0,0, SAW_BASE, FM_OFF, 0, 0, 0,0,0,0},
{4,2,1,0,0,0,0,0, SAW_BASE, FM_OFF, 0, 0, 0,0,0,0},
{8,2,1,0,0,0,0,0, SAW_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*24*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*32*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*40*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*48*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*56*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*64*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*72*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*80*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*88*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*96*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*104*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*112*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},/*120*/
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},

{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0},
{1,0,0,0,0,0,0,0, SINE_BASE, FM_OFF, 0, 0, 0,0,0,0}
};