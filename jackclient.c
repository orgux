/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* jack test of synth */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "orgux.h"

#include <jack/jack.h>
#include <jack/midiport.h>
jack_port_t *input_port;
jack_port_t *output_port_1;
jack_port_t *output_port_2;

orgux_state* S;

int process (jack_nframes_t nframes, void *arg) {
  int i;
  void* port_buf = jack_port_get_buffer(input_port, nframes);

  jack_default_audio_sample_t *out1 = (jack_default_audio_sample_t *)
jack_port_get_buffer(output_port_1, nframes);

  jack_default_audio_sample_t *out2 = (jack_default_audio_sample_t *)
jack_port_get_buffer(output_port_2, nframes);

  jack_midi_event_t in_event;
  jack_nframes_t event_count = jack_midi_get_event_count(port_buf);

  unsigned char midi[3];
  for(int i=0; i< event_count; i++){

    jack_midi_event_get(&in_event, port_buf, i);
    midi[0] = in_event.buffer[0];
    midi[1] = in_event.buffer[1];
    midi[2] = in_event.buffer[2];

    orgux_schedule(S, in_event.time, midi);

  }

  orgux_process(S, out1, out2, nframes);

  return 0;      
}

int
srate (jack_nframes_t nframes, void *arg)

{
        printf ("the sample rate is now %lu/sec\n", nframes);
        return 0;
}


void
jack_shutdown (void *arg)
{
  printf("jack shutdown\n");
  exit (1);
}

void
error (const char *desc)
{
        fprintf (stderr, "JACK error: %s\n", desc);
}



int main(){



  jack_client_t *client;

  jack_set_error_function (error);

  if ((client = jack_client_new ("orgux")) == 0) {
    fprintf (stderr, "jack server not running?\n");
    return 1;
  }

  S = orgux_init(jack_get_sample_rate(client));

  jack_set_process_callback (client, process, 0);
  jack_set_sample_rate_callback (client, srate, 0);
  jack_on_shutdown (client, jack_shutdown, 0);

  input_port = jack_port_register(client, "midi-in",
JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
  output_port_1 = jack_port_register (client, "out-1",
JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  output_port_2 = jack_port_register (client, "out-2",
JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

  if (jack_activate (client)) {
    fprintf (stderr, "cannot activate client");
    return 1;
  }

  while(1){
    sleep(1);
  }

  orgux_free(S);

  jack_client_close (client);
  return 0;
}