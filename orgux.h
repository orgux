/* orgux.h */

/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

typedef struct orgux_state orgux_state;

typedef struct {
  int (*read)(unsigned char* buf, int count, void* ud);
  void* ud;
} orgux_reader;

void orgux_process(orgux_state* S, float* lbuf, float* rbuf, int len);
void orgux_schedule(orgux_state* S, int sample, unsigned char midi[3]);
void orgux_control(orgux_state* S, int type, int chan, int val1, int val2);
orgux_state* orgux_init(int sample_rate);
void orgux_free(orgux_state* S);

int orgux_load_smf(orgux_state* S, char* filename);
int orgux_load_song(orgux_state* S, orgux_reader* src);
void orgux_play(orgux_state* S, int yesno);
void orgux_set_position(orgux_state* S, int tick);
void orgux_set_volume(orgux_state* S, float level);
void orgux_set_tempo(orgux_state* S, float bpm);
void orgux_fade_out(orgux_state* S, float speed);
void orgux_fade_in(orgux_state* S, float speed);