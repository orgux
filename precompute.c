/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* precompute waveforms */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "constants.h"

extern float square_wave[WAVEFORM_LENGTH];
extern float saw_wave[WAVEFORM_LENGTH];
extern float triangle_wave[WAVEFORM_LENGTH];

extern const unsigned char instr_config[128][16];

extern float waveform[128][WAVEFORM_LENGTH];
extern float waveform_M[128][WAVEFORM_LENGTH];
extern float waveform_Z[128][WAVEFORM_LENGTH];

extern float samples[SAMPLE_COUNT][SAMPLE_LENGTH];
extern int sample_len[SAMPLE_COUNT];
extern float antipop[ANTIPOP_LENGTH];

float linterp(float x, int k, float wave[]){

  float X = x*k;
  float Xw = X - WAVEFORM_LENGTH*floor(X/WAVEFORM_LENGTH);
  int X0 = (int)floor(Xw);
  int X1 = X0+1;
  if(X1 == WAVEFORM_LENGTH) X1 = 0;

  float y1 = wave[X1];
  float y0 = wave[X0];
  float m = (y1-y0)/(X1-X0);

  return m*(Xw-X0)+y0;
}

void precompute(int sample_rate){

  float dt = 1.0/sample_rate;

printf("generating band limited bases... ");

  for(int i=0; i<WAVEFORM_LENGTH; i++){
    square_wave[i] = 0;
    triangle_wave[i] = 0;
    saw_wave[i] = 0;
    for(int j=0; j<7; j++){
      square_wave[i] += (4/PI)*sin(i*(2*j+1)*PI2/WAVEFORM_LENGTH)/(2*j+1);
      triangle_wave[i] += (8/(PI*PI))*(j&1?1:-1)*sin(i*(2*j+1)*PI2/WAVEFORM_LENGTH)/((2*j+1)*(2*j+1));
      saw_wave[i] += (2/PI)*sin(i*(j+1)*PI2/WAVEFORM_LENGTH)/(j+1);
    }
  }


//float maxf[3] = {0,0,0};
//for(int i=0; i<WAVEFORM_LENGTH; i++){
//  if(maxf[0] < square_wave[i]) maxf[0] = square_wave[i];
//  if(maxf[1] < triangle_wave[i]) maxf[1] = triangle_wave[i];
//  if(maxf[2] < saw_wave[i]) maxf[2] = saw_wave[i];
//}

for(int i=0; i<WAVEFORM_LENGTH; i++){
  square_wave[i] /= 2;
  triangle_wave[i] /= 1;
  saw_wave[i] /= 1;
}

//printf("\nsquare max = %f\n",maxf[0]);
//printf("triangle max = %f\n",maxf[1]);
//printf("saw max = %f\n\n",maxf[2]);


printf("OK\n");

  /* setup wave tables */

printf("generating instruments... \n");

printf("      harmonic  0   1   2   3   4   5   6   7 |basis FM  FM_freq LP  HP\n");
  for(int i=0; i<128; i++){

int max = 0;
float N = 0;

printf("instr[%3d] = {",i);

    for(int j=0; j<HARMONIC_COUNT; j++){
      printf("%3d ",instr_config[i][j]);
      if(instr_config[i][j] > max){ max = instr_config[i][j]; }
    }

printf("|");

    switch(instr_config[i][BASE_CONFIG]){
      case SINE_BASE: printf("SINE  "); break;
      case SQUARE_BASE: printf("PULSE "); break;
      case SAW_BASE: printf("SAW   "); break;
      case TRIANGLE_BASE: printf("TRI   "); break;
      case NOISE_BASE: printf("NOISE "); break;
      default: printf("%5d ",instr_config[i][BASE_CONFIG]); break;
    }

    for(int j=0; j<HARMONIC_COUNT; j++){
      if(instr_config[i][j]==0) continue;
      float M = ((float)instr_config[i][j])/max;
      M = M*M;
      N += M;

      switch(instr_config[i][BASE_CONFIG]){
        case SINE_BASE: 
          for(int k=0; k<WAVEFORM_LENGTH; k++){
            waveform[i][k]+=M*sin(k*(j+1)*PI2/WAVEFORM_LENGTH);
          }
          break;
        case SQUARE_BASE: 
          for(int k=0; k<WAVEFORM_LENGTH; k++){
            waveform[i][k]+=M*linterp(k,j+1,square_wave);;
          }
          break;
        case SAW_BASE: 
          for(int k=0; k<WAVEFORM_LENGTH; k++){
            waveform[i][k]+=M*linterp(k,j+1,saw_wave);;
          }
          break;
        case TRIANGLE_BASE: 
          for(int k=0; k<WAVEFORM_LENGTH; k++){
            waveform[i][k]+=M*linterp(k,j+1,triangle_wave);
          }
          break;
      }

    }

    if(N>0){
      for(int j=0; j<WAVEFORM_LENGTH; j++){
        waveform[i][j] /= N*10;
      }
    }

/*
    for(int j=0; j<WAVEFORM_LENGTH; j++){
      waveform[i][j] = 0.0f;
      N = 0;
      for(int k=0; k<HARMONIC_COUNT; k++){
        float M = ((float)instr_config[i][k])/max;
        M = M*M;
        N += M;

        float y;
        switch(instr_config[i][BASE_CONFIG]){
          case SINE_BASE: y = sin(j*(k+1)*PI2/WAVEFORM_LENGTH); break;
          //case SQUARE_BASE: y = ((2*j*(1<<k)/WAVEFORM_LENGTH)&1)?1:-1; break;
          case SQUARE_BASE: y = linterp(j,k+1,square_wave); break;
          case TRIANGLE_BASE: y = linterp(j,k+1,triangle_wave); break;
          case SAW_BASE: y = linterp(j,k+1,saw_wave); break;
          //case NOISE_BASE: y = RANDF()*2 - 1; break;
          case NOISE_BASE: y = 0; break;
          default: y = 0;//use previously defined waveform
        }

        waveform[i][j] += M*y;
      }
      if(N>0){ waveform[i][j] /= N*10; }
    }
*/
    printf("\n");

  }

  printf("OK\n");

  //precomputing linear interpolators
  printf("precomputing linear interpolators...\n");
  for(int j=0; j<128; j++){
    for(int i=0; i<WAVEFORM_LENGTH; i++){

      int X0 = i;
      int X1 = i+1;
      if(X1==WAVEFORM_LENGTH){X1 = 0;}
      waveform_M[j][i] = waveform[j][X1] - waveform[j][X0];
      waveform_Z[j][i] = waveform[j][X0] - i*waveform_M[j][i];

    }
  }


  //generate antipop cosine
  for(int i=0; i<ANTIPOP_LENGTH; i++){
    antipop[i] = -cos(i*PI2/(2.0*ANTIPOP_LENGTH))/2.0+0.5;
  }


  //generate samples

  float par[16][8] = {/**/
{50, 500, 0.5, 10000, 0, 0, 0, 0},
{200, 200, 0.5, 10000, 0, 0, 0, 0},
{100, 500, 0.5, 10000, 0, 0, 0, 0},
{100, 1000, 0.5, 10000, 0, 0, 0, 0},

{100, 500, 0.5, 3000, 0, 0, 0, 0},
{200, 500, 0.5, 3000, 0, 0, 0, 0},
{300, 500, 0.5, 3000, 0, 0, 0, 0},
{400, 500, 0.5, 3000, 0, 0, 0, 0},

{400, 200, 0.5, 3000, 0, 0, 0, 0},
{400, 100, 0.5, 3000, 0, 0, 0, 0},
{400, 50, 0.5, 3000, 0, 0, 0, 0},
{400, 20, 0.5, 3000, 0, 0, 0, 0},

{100, 500, 0.5, 10000, 0, 0, 0, 0},
{100, 500, 0.5, 10000, 0, 0, 0, 0},
{100, 500, 0.5, 10000, 0, 0, 0, 0},
{100, 500, 0.5, 10000, 0, 0, 0, 0}
};

  double K = PI2 / sample_rate;
printf("generating samples... ");
  for(int i=0; i<SAMPLE_LENGTH; i++){
    for(int j=0; j<SAMPLE_COUNT; j++){
      double F_i = par[j][0];
      double F_r = par[j][1];
      double A_i = par[j][2];
      double A_r = par[j][3];

      if(F_r < 1){F_r = 1;}
      double f = F_i - i/F_r;
      if(f < 0) f = 0;

      if(A_r < 1){A_r = 1;}
      double A = A_i - i/A_r;
      if(A < 0) A = 0;

      double E = 1;
      if(i < 64){
        E = -cos((i/64.0) * PI2 / 2)/2+0.5;
      }

      samples[j][i] = E*A*sin(i*f*K);
    }
  }



  float y_l;
  float y_h0=0;
  float y_h1=0;
  
/*
notes

cool hit
tau2 = 0.0001 / (1 + i/50.0)
alpha2 = tau2 / (tau2 + dt)
A = 0.2 - (i/10000.0)





*/

  for(int i=0; i<SAMPLE_LENGTH; i++){

    float tau1 = (i/200.0)*0.0001;
    float alpha1 = dt/(tau1+dt);

    float tau2 = 0.0001 / (1 + i/50.0);
    float alpha2 = tau2/(tau2+dt);

    float y = RAND_SIGNAL();

    //y = y_l0 + alpha1*(y - y_l0);
    //y_l0 = y;

    float temp = y;
    y = alpha2*(y - y_h0 + y_h1);
    y_h0 = temp;
    y_h1 = y;

    float A = 0.2 - (i/10000.0);
    if(A<0) A = 0;

    double E = 1;
    if(i < 64){
      E = -cos((i/64.0) * PI2 / 2)/2+0.5;
    }

    samples[15][i] = A*E*y;

  }

  for(int j=0; j<SAMPLE_COUNT; j++){
    for(int i=0; i<SAMPLE_LENGTH; i++){
      int I = SAMPLE_LENGTH-i-1;
      if(fabs(samples[j][I]) != 0){
        sample_len[j] = I;
        break;
      }
    }
  }

printf("OK\n");

printf("precompute OK\n");
  

}