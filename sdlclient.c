/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* sdl test */


#include <SDL/SDL.h>
#include "orgux.h"


#define SAMPLE_RATE 44100
#define SAMPLE_COUNT 1024
#define BUFFER_SIZE (SAMPLE_COUNT*2)


SDL_AudioSpec audio;

orgux_state* S;

void audio_callback(void *userdata, Uint8 *stream, int len){

  Sint16* out = (Sint16*)stream;
  float lbuf[SAMPLE_COUNT];
  float rbuf[SAMPLE_COUNT];

  orgux_process(S, lbuf, rbuf, SAMPLE_COUNT);

  for(int i=0; i<SAMPLE_COUNT; i++){
    out[2*i] = (lbuf[i]*32767);
    out[2*i+1] = (rbuf[i]*32767);
  }

}

int pull(unsigned char* buf, int count, void* ud){
  FILE* f = (FILE*)ud;
  int n = fread(buf,1,count,f);
  if(n < count) return -1;
  return 0;
}



int main(int argc, char* argv[]){
  
  S = orgux_init(SAMPLE_RATE);

  if(orgux_load_smf(S, "song.mid") < 0){
    printf("cannot open midi file\n");
    exit(-1);
  }

  audio.freq = SAMPLE_RATE;
  audio.format = AUDIO_S16;
  audio.channels = 2;
  audio.samples = BUFFER_SIZE/2;
  audio.callback = audio_callback;

  if(SDL_OpenAudio(&audio, NULL)<0){
    printf("cannot open audio: %s\n",SDL_GetError());
    exit(-1);
  }

  SDL_PauseAudio(0);
  SDL_Delay(1000);

  orgux_play(S, 1);

  while(1){
    SDL_Delay(1000);
  }

  return 0;
}
