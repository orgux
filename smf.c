/*
orgux - a just-for-fun real time synth
Copyright (C) 2009  Evan Rinehart

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* smf */

#include <stdio.h>
#include <string.h>

#include "orgux.h"

#define STRING_MAX 256

typedef unsigned char octet;

extern void orgux_seq_append(orgux_state* S, int tick, int type, int chan, int val1, int val2);
extern void orgux_seq_sort(orgux_state* S);

int bytes2short(octet arr[2]){
  return (arr[0]<<8) | arr[1];
}

int bytes2int(octet arr[4]){
  return (arr[0]<<24) | (arr[1]<<16) | (arr[2]<<8) | arr[3];
}


/* read a bunch of bytes and discard */
void skip_ahead(orgux_reader* src, int count){
  const int W = 256;
  char buf[W];

  int C = count;

  while(C > W){
    src->read(buf,W,src->ud);
    C -= W;
  }
  src->read(buf,C,src->ud);
}

octet read_one(orgux_reader* src){
  octet byte[1];
  src->read(byte,1,src->ud);
  return byte[0];
}

int get_delta(orgux_reader* src){
  octet a = read_one(src);
  if(a<0x80){return a;}

  octet b = read_one(src);
  if(b<0x80){return ((a&0x7f)<<7) | b;}

  octet c = read_one(src);
  if(c<0x80){return ((a&0x7f)<<14) | ((b&0x7f)<<7) | c;}

  octet d = read_one(src);
  return ((a&0x7f)<<21) | ((b&0x7f)<<14) | ((c&0x7f)<<7) | d;
}


int orgux_load_song(orgux_state* S, orgux_reader* src){

  char buf[16];
  char string[STRING_MAX];
  int e;
  int n;

  int loopstart;
  int loopend;
  int uspq;

  /*MThd*/
  if(src->read(buf,4,src->ud)<0) return -1;
printf("%x %x %x %x\n",buf[0],buf[1],buf[2],buf[3]);
  /*0x00000006*/
  if(src->read(buf,4,src->ud)<0) return -1;
printf("%x %x %x %x\n",buf[0],buf[1],buf[2],buf[3]);
  /*format type: 0x0000 0x0001 or 0x0002*/
  if(src->read(buf,2,src->ud)<0) return -1;
printf("%x %x\n",buf[0],buf[1]);
  /*number of tracks*/
  if(src->read(buf,2,src->ud)<0) return -1;
  int track_count = bytes2short(buf);
printf("%x %x\n",buf[0],buf[1]);
printf("track count = %d\n",track_count);
  /* time division */
  if(src->read(buf,2,src->ud)<0) return -1;
  //code to figure out time division
printf("%x %x\n",buf[0],buf[1]);
  for(int i=0; i<track_count; i++){

    /*MTrk*/
    if(src->read(buf,4,src->ud)<0) return -1;
printf("%x %x %x %x\n",buf[0],buf[1],buf[2],buf[3]);
    /* chunk size */
    if(src->read(buf,4,src->ud)<0) return -1;
    int chunk_size = bytes2int(buf);
printf("%x %x %x %x\n",buf[0],buf[1],buf[2],buf[3]);
printf("chunk size = %d\n",chunk_size);
    int tick = 0;
    int end_of_track = 0;
    int last_type = 0x80;
    int last_chan = 0;
    while(1){
      int delta = get_delta(src);
printf("delta = %d\n",delta);
      if(delta < 0) return -1;
      tick += delta;

      //type and channel
      if(src->read(buf,1,src->ud)<0) return -1;
      int type = buf[0] & 0xf0;
      if(type >= 0x80 && type <= 0xe0){//normal event
        last_type = type;
        int chan = buf[0] & 0x0f;
        last_chan = chan;
        if(src->read(buf,2,src->ud)<0) return -1;
        int val1 = buf[0];
        int val2 = buf[1];
printf("normal %x %x %x\n",type|chan,val1,val2);
        orgux_seq_append(S,tick,type,chan,val1,val2);
      }
      else if(type < 0x80){//running status
        int val1 = buf[0];
        if(src->read(buf,1,src->ud)<0) return -1;
        int val2 = buf[0];
printf("running %x %x %x\n",last_type|last_chan,val1,val2);
        orgux_seq_append(S,tick,last_type,last_chan,val1,val2);
      }
      else if(type == 0xff){//meta event
        if(src->read(buf,1,src->ud)<0) return -1;
        type = buf[0];
printf("meta event\n");
        int len = get_delta(src);
        if(len < 0) return -1;

        switch(type){
          case 0x2f: printf("end of track\n"); end_of_track = 1; break;
          case 0x51:printf("tempo change\n");/*tempo*/
            if(src->read(buf,3,src->ud)<0) return -1;
            uspq = (buf[0]<<16) | (buf[1]<<8) | buf[0];
            break;
          case 0x58: printf("time sig\n");/*time signature*/
            if(src->read(buf,4,src->ud)<0) return -1;
            //...
            break;
          case 0x01: printf("text\n");/*text*/
            if(len >= STRING_MAX){skip_ahead(src, len);}
            else{
              if(src->read(string,len,src->ud)<0) return -1;
              string[len] = '\0';
              if(strncmp(string,"LoopStart",len)==0){
                loopstart = tick;
              }
              else if(strncmp(string,"LoopEnd",len)==0){
                loopend = tick;
              }
            }

            break;
          default: skip_ahead(src,len); break;
        }
      }
      else{printf("sysex\n");//sysex and such...
        int len = get_delta(src);
        if(len < 0) return -1;
        skip_ahead(src, len);
      }

      if(end_of_track) break;
    }

  }

  orgux_seq_sort(S);

  //orgux_set_tempo();
  //orgux_set_loop_positions(S, loopstart, loopend);

  return 0;

}


int read_file(unsigned char* buf, int count, void* ud){
  FILE* f = (FILE*)ud;
  int n = fread(buf, 1, count, f);
  if(n < count){return -1;}
  return 0;
}

int orgux_load_smf(orgux_state* S, char* filename){
  FILE* f = fopen(filename, "r");
  if(!f){return -1;}
  orgux_reader src;
  src.ud = f;
  src.read = read_file;
printf("loading midi file\n");
  if(orgux_load_song(S, &src) < 0){fclose(f); return -1;}
  fclose(f);
  return 0;
}
